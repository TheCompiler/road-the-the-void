//! @file
//! Файл технических настроек
//===========================================================================================================

#ifndef GAME_CONFIG_H
#define GAME_CONFIG_H

#include "Logger.h"
#include "Exceptions.h"
#include "SFML/System/Vector2.hpp"
#include "SFML/System/Vector3.hpp"
#include "SFML/Graphics/CircleShape.hpp"

#define X_SCALE 1.4545
#define Y_SCALE 6

#define DEBUG        ///< (Для разработки) включает записи игровых событий
#define ALLOW_SHOOT  ///< (Для разработки) разрешает стрелять ботам
#define MAX_DMG -1   ///< Максимально возможный урон в игре (уничтажает любой объект)

#define TIME 800
#define SCREEN_RESOLUTION_X 1920
#define SCREEN_RESOLUTION_Y 1080
#define X_BACKGROUND 1920
#define Y_BACKGROUND 1080
#define SCALE_FACTOR 1.5

#define DIRECTORY_OF_TEXTURE std::string("Game/img/")

#define ENEMY_RADIUS_ATTACK 500
#define ENEMY_SPEED_ANGLE 0.01
#define SAFE_DISTANSE_FOR_ENEMY 150

typedef sf::Vector2<double> Vector2d;
typedef sf::Vector3<double> Vector3d;

#endif //GAME_CONFIG_H
