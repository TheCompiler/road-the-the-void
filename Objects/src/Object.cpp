#include "Object.h"

#include <utility>

Object::Object(math::Coords coords, double angle, double scale, double speed,
               life_length length, size_t texture, sf::CircleShape shape)
      : _id_texture(std::move(texture))
        , _angle(angle)
        , _scale(scale)
        , _speed(speed)
        , _condition(ALIVE)
        , _angle_speed(0)
        , _coords(coords)
        , _life_len(length)
        , _collision_polygon(std::move(shape)) {
    _collision_polygon.setRadius(
          std::min(Textures::_textures[_id_texture].getSize().y,
                   Textures::_textures[_id_texture].getSize().x) * _scale / 2.0);
    _collision_polygon.setPosition(_coords.x, _coords.y);
    _collision_polygon.setOrigin(_collision_polygon.getRadius(), _collision_polygon.getRadius());
}

Object::Object(double x, double y, double angle, double scale,
               life_length length, size_t texture, sf::CircleShape shape)
      : _id_texture(std::move(texture))
        , _angle(angle)
        , _scale(scale)
        , _speed(0)
        , _condition(ALIVE)
        , _angle_speed(0)
        , _coords(x, y)
        , _life_len(length)
        , _collision_polygon(std::move(shape)) {
    _collision_polygon.setRadius(
          std::min(Textures::_textures[_id_texture].getSize().y,
                   Textures::_textures[_id_texture].getSize().x) * _scale / 2.0);
    _collision_polygon.setPosition(_coords.x, _coords.y);
    _collision_polygon.setOrigin(_collision_polygon.getRadius(), _collision_polygon.getRadius());
}

Object::Object(const pugi::xml_node_iterator _root)
      : _id_texture(_root->child("Draw").child("Texture").child("id").text().as_ullong())
        , _angle(_root->child("Transform").child("Angle").text().as_double())
        , _scale(_root->child("Transform").child("Scale").text().as_double())
        , _speed(_root->child("Properties").child("Move").child("Start_speed").text().as_double())
        , _condition(_root->child("Properties").child("State").child("Condition").text().as_int())
        , _angle_speed(
            _root->child("Properties").child("Move").child("Angle_speed").text().as_double())
        , _coords(_root->child("Coords").child("x").text().as_double(),
                  _root->child("Coords").child("y").text().as_double())
        , _life_len(static_cast<life_length>(_root->child("Properties").child("State").child(
            "Life_len").text().as_int()))
        , _collision_polygon(0,
                             _root->child("Draw").child("Shape").child(
                                   "Nodes").text().as_ullong()) {
    _collision_polygon.setRadius(
          std::min(Textures::_textures[_id_texture].getSize().y,
                   Textures::_textures[_id_texture].getSize().x) * _scale / 2.0);
    _collision_polygon.setPosition(_coords.x, _coords.y);
    _collision_polygon.setOrigin(_collision_polygon.getRadius(), _collision_polygon.getRadius());
}

double Object::getAngle() const {
    return _angle;
}

double Object::getScale() const {
    return _scale;
}

math::Coords Object::getCoords() const {
    return _coords;
}

const uint8_t Object::getCondition() const {
    return _condition;
}

const double Object::getSpeed() const {
    return _speed;
}

const life_length Object::getLifeLen() const {
    return _life_len;
}

const sf::CircleShape &Object::getShape() const {
    return _collision_polygon;
}

const size_t Object::getIdTexture() const {
    return _id_texture;
}

void Object::rotate(double angle) {
    _angle += angle;
    while (fabs(_angle) > 2 * M_PI) {
        if (_angle > 0) {
            _angle -= 2 * M_PI;
        } else {
            _angle += 2 * M_PI;
        }
    }
    _collision_polygon.rotate(_angle);
}

void Object::draw(sf::RenderWindow &window) const {
    sf::Sprite sprite;
    sprite.setTexture(Textures::_textures[_id_texture]);
    sprite.setPosition(_coords.x, -_coords.y);
    sprite.setScale(_scale, _scale);
    sprite.setOrigin((sf::Vector2f) Textures::_textures[_id_texture].getSize() / 2.0f);
    sprite.setRotation(math::Math::to_degrees(_angle));
    window.draw(sprite);
}

void Object::setCondition(uint8_t condition) {
    _condition = condition;
}

void Object::setCoords(double x, double y) {
    _collision_polygon.setPosition(x, y);
    _coords.x = x;
    _coords.y = y;
}

void Object::setCoords(math::Coords coords) {
    setCoords(coords.x, coords.y);
}

void Object::setCoords(Vector2d vec) {
    setCoords(vec.x, vec.y);
}

void Object::changeScale(double diff) {
    _scale *= diff;
}

void Object::save(const pugi::xml_node_iterator _root) const {
    _root->append_child("Transform").append_child("Angle").set_value(
          std::to_string(_angle).c_str());
    _root->child("Transform").append_child("Scale").set_value(std::to_string(_scale).c_str());
    _root->append_child("Properties").append_child("Move").set_value(
          std::to_string(_speed).c_str());
    _root->child("Properties").append_child("State").set_value(std::to_string(_condition).c_str());
    _root->child("Properties").append_child("Angle_speed").set_value(
          std::to_string(_angle_speed).c_str());
    _root->child("Properties").append_child("State").set_value(std::to_string(_life_len).c_str());
    _root->append_child("Coords").append_child("x").set_value(std::to_string(_coords.x).c_str());
    _root->child("Coords").append_child("y").set_value(std::to_string(_coords.y).c_str());
    _root->append_child("Draw").append_child("Shape").append_child("Nodes").set_value(
          std::to_string(_collision_polygon.getPointCount()).c_str());
    _root->child("Draw").append_child("Texture").append_child("id").set_value(
          std::to_string(_id_texture).c_str());
}

