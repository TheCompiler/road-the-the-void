#include "Enemy.h"

using math::Alg_vec2;

Enemy::Enemy(double x, double y, double angle, double scale,
             double hp, double dmg, double acceleration, double max_speed, SceneT &scene,
             size_t texture, sf::CircleShape shape)
      : Player(x, y, angle, scale, hp, dmg, acceleration, max_speed, texture, shape)
        , _scene(scene) {}

Enemy::Enemy(const pugi::xml_node_iterator _root, SceneT *scene)
        : Player(_root), _scene(*scene) {}

void Enemy::move(move_dir dir) {
    if (!_condition) return;

    Player player = _scene.getPlayer();
    math::Alg_vec2 dir_player = Alg_vec2(player.getAngle()) * player.getSpeed();
    math::Coords player_cord = player.getCoords();
    math::Coords my_cords = getCoords();

    if ((sq(player_cord.x - my_cords.x) +
         sq(player_cord.y - my_cords.y)) > sq(ENEMY_RADIUS_ATTACK)) {
        return;
    }

    gamelog("AI action: found Player");

    auto dir_vec = Alg_vec2(getAngle());
    Alg_vec2 vec_to_player = math::Math::anticipation(player.getCoords(), dir_player,
                                                      my_cords, 1);
    double angle_to_player = math::Math::shortest_rotate_vec(dir_vec, vec_to_player);


    if (fabs(angle_to_player) <= ENEMY_SPEED_ANGLE) {
        gamelog("AI action: targeted to Player");

        int danger = check_danger(vec_to_player);
        if (danger) {
            gamelog("AI action: found danger");
            Player::rotate(-1 * danger * _angle_speed);
        } else {
#ifdef ALLOW_SHOOT
            shot(_scene);
#endif
        }
        Player::move(MOVE_UP);
        return;
    }

    gamelog("AI action: targeting to Player");
    if (fabs(_angle_speed) > fabs(angle_to_player)) {
        Player::rotate(angle_to_player);
    } else {
        Player::rotate((angle_to_player > 0 ? 1 : -1) * _angle_speed);
    }
}

int Enemy::check_danger(Alg_vec2 dir_enemy) const {
    std::vector<std::shared_ptr<Object>> objects = _scene.getObjects();

    for (auto object : objects) {
        if (getCoords() == object->getCoords()) continue;
        auto to_object = math::Alg_vec2(getCoords(), object->getCoords());

        if ((to_object ^ dir_enemy) < M_PI_2 &&
            to_object.squared_len() < sq(SAFE_DISTANSE_FOR_ENEMY)) {
            int ans = math::Math::colide(getShape(), dir_enemy,
                                         object->getShape());

            if (ans != 0) {
                return ans;
            }
        }
    }

    return 0;
}

void Enemy::setNEnemyes(size_t count) {
    _count_of_enemies = count;
}

size_t Enemy::getNEnemyes() {
    return _count_of_enemies;
}

size_t Enemy::_count_of_enemies = 0;

void Enemy::getDmg(double dmg) {
    if (!_condition) return;
    Player::getDmg(dmg);
    if (!_condition) {
        _count_of_enemies--;
        Player::addScore(60);
    } else {
        Player::addScore(20);
    }
}

void Enemy::save(const pugi::xml_node_iterator _root) const {
    if (_root->child("Enemies").empty()) {
        _root->append_child("Enemies");
        _root->child("Enemies").append_attribute("count").set_value(0);
    }
    _root->child("Enemies").attribute("count").set_value(
          _root->child("Enemies").attribute("count").as_uint() + 1);
    _root->child("Enemies").append_child("Enemy");
    Player::save(_root->child("Enemies").child("Enemy"));
}

