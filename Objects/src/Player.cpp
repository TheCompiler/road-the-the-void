#include "Player.h"

Player::Player(double x, double y, double angle, double scale,
               double hp, double dmg, double acceleration, double max_speed,
               size_t texture, sf::CircleShape shape)
      : Object(x, y, angle, scale, MEDIUM_LIFE, texture, shape)
        , _hp(hp)
        , _max_speed(max_speed)
        , _acceleration(acceleration)
        , _name_weapon("Player")
        , _weapon(dmg) {}

Player::Player(const pugi::xml_node_iterator _root)
      : Object(_root)
        , _hp(_root->child("Properties").child("State").child("HP").text().as_llong())
        , _max_speed(_root->child("Properties").child("Move").child("Max_speed").text().as_double())
        , _acceleration(
            _root->child("Properties").child("Move").child("Acceleration").text().as_double())
        , _name_weapon(_root->child("Weapon").child("Name").text().as_string())
        , _weapon(Weapons::_weapons[_root->child("Weapon").child("Name").text().as_string()]) {}

void Player::move(move_dir dir) {
    if (!_condition) return;
    switch (dir) {
        case MOVE_UP:
            changeSpeed(1);
            break;
        case MOVE_DOWN:
            changeSpeed(-2);
            break;
        case MOVE_LEFT:
            rotate(-_angle_speed);
            break;
        case MOVE_RIGHT:
            rotate(_angle_speed);
            break;
        case STAYING:
            changeSpeed(-1);
            break;
        default:
            break;
    }
    if (_speed) {
        _condition |= MOVED;
    }
}

void Player::getDmg(double dmg) {
    if (!_condition) return;
    _hp -= dmg;
    _condition |= DAMAGED;
    if (_hp <= 0 || dmg == MAX_DMG) {
        _condition &= DEAD;
    }
    gamelog("Game action: Player or Enemy got damage. hp = " + atof(_hp));
}

void Player::changeSpeed(int dir) {
    if (dir < 0) {
        if (_acceleration > _speed) {
            _speed = 0;
        }
    } else if (dir > 0) {
        if (_speed + _acceleration > _max_speed) {
            _speed = _max_speed;
        }
    }

    _speed = dir * _acceleration + _speed;
    math::Alg_vec2 vec(getAngle());
    vec = vec * _speed;
    _coords.x += vec.x();
    _coords.y += vec.y();
    _collision_polygon.move(vec.x(), vec.y());

    gamelog("Game action: Player or Enemy moved to: ("
            + atof(vec.x()) + ";" + atof(vec.y()) + ")");
}

void Player::giveDmg(Hittable &object) const {
    _weapon.giveDmg(object);
}

const Weapon &Player::getWeapon() const {
    return _weapon;
}

void Player::shot(SceneT &scene) const {
    if (!_condition) return;
    auto dir_vec = math::Alg_vec2(_angle)
                   * (_collision_polygon.getRadius() + _weapon.getRadiusBullet() + 25);
    _weapon.shoot(&scene, {_coords.x + dir_vec.x(), _coords.y + dir_vec.y()},
                  _angle, _speed);
}

const double Player::getHp() const {
    return _hp;
}

unsigned long long Player::_score = 0;

unsigned long long Player::getScore() {
    return _score;
}

void Player::setScore(unsigned long long score) {
    _score = score;
}

void Player::addScore(unsigned long long reward) {
    _score += reward;
}

void Player::save(const pugi::xml_node_iterator _root) const {
    Object::save(_root);
    _root->child("Properties").child("State").append_child("HP").set_value(
          std::to_string(_hp).c_str());
    _root->child("Properties").child("Move").append_child("Max_speed").set_value(
          std::to_string(_max_speed).c_str());
    _root->child("Properties").child("Move").append_child("Acceleration").set_value(
          std::to_string(_acceleration).c_str());
    _root->append_child("Weapon").append_child("Name").set_value(_name_weapon.c_str());
}
