#include "Bullet.h"
#include "Scene.h"
#include "Weapon.h"

void Weapon::shoot(SceneT *scene, math::Coords coords, double angle, double speed) const {
    std::shared_ptr<Bullet> bullet(
          new Bullet(coords, angle, 1, speed + _speed_bullet, 10,
                     _id_texture_bullet, sf::CircleShape(_radius_bullet)));
    scene->addObject(bullet);
}

Weapon::Weapon(double dmg)
      : _dmg(dmg)
        , _radius_bullet(5)
        , _speed_bullet(10)
        , _id_texture_bullet(4) {}

void Weapon::giveDmg(Hittable &object) const {
    object.getDmg(_dmg);
}

double Weapon::getRadiusBullet() const {
    return _radius_bullet;
}

Weapon::Weapon(const pugi::xml_node_iterator _root)
      : _dmg(_root->child("Damage").text().as_double())
        , _radius_bullet(_root->child("Bullet_radius").text().as_double())
        , _speed_bullet(_root->child("Bullet_speed").text().as_double())
        , _id_texture_bullet(_root->child("Texture").child("id").text().as_ullong()) {}

void Weapon::save(const pugi::xml_node_iterator _root) const {
    _root->append_child("Damage").set_value(std::to_string(_dmg).c_str());
    _root->append_child("Bullet_radius").set_value(std::to_string(_radius_bullet).c_str());
    _root->append_child("Bullet_speed").set_value(std::to_string(_speed_bullet).c_str());
    _root->append_child("Bullet_texture").append_child("id").set_value(
          std::to_string(_id_texture_bullet).c_str());
}
