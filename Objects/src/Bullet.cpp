#include "Bullet.h"

Bullet::Bullet(const pugi::xml_node_iterator _root)
      : Object(_root)
        , _dmg(_root->child("Properties").child("State").child("Damage").text().as_double()) {}

Bullet::Bullet(math::Coords coords, double angle, double scale, double speed, double dmg,
               size_t texture, sf::CircleShape shape)
      : Object(coords, angle, scale, speed, SHORT_LIFE, texture, shape)
        , _dmg(dmg) {
    _condition |= MOVED;
}

void Bullet::move(enum move_dir dir) {
    math::Alg_vec2 vec(getAngle());
    vec = vec * _speed;
    _coords.x += vec.x();
    _coords.y += vec.y();
    _collision_polygon.move(vec.x(), vec.y());
    gamelog("Game action: bullet moved to: (" + atof(vec.x()) + ";" + atof(vec.y()) + ")");
}

void Bullet::getDmg(double dmg) {
    if (!_condition) return;
    _condition &= DEAD;
    gamelog("Game action: bullet destroyed");
}

void Bullet::giveDmg(Hittable &object) const {
    object.getDmg(_dmg);
}

void Bullet::save(const pugi::xml_node_iterator _root) const {
    if (_root->child("Bullets").empty()) {
        _root->append_child("Bullets");
    }
    _root->child("Bullets").append_child("Bullet");
    Object::save(_root->child("Bullets").child("Bullet"));
    _root->child("Properties").child("State").append_child("Damage").set_value(
          std::to_string(_dmg).c_str());
}
