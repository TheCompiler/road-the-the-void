#include "Asteroid.h"

Asteroid::Asteroid(math::Coords coords, double angle, double scale, double speed, int life_count,
                   size_t texture, sf::CircleShape shape)
      : Object(coords, angle, scale, speed, MEDIUM_LIFE, texture, shape)
        , _delta_scale((1.0 / life_count) * _scale)
        , _life_count(life_count) {
    _condition |= MOVED;
}

Asteroid::Asteroid(const pugi::xml_node_iterator _root)
      : Object(_root)
        , _life_count(_root->child("Properties").child("State").child(
            "Life_count").text().as_int())
        , _delta_scale((1.0 / _root->child("Properties").child("State").child(
            "Life_count").text().as_int()) * _scale) {}

void Asteroid::move(enum move_dir dir) {
    if (!_condition) return;
    if (dir == MOVE_UP) {
        rotate(ASTEROID_ANGLE * _angle_speed);
    } else if (dir == MOVE_DOWN) {
        rotate(-ASTEROID_ANGLE * _angle_speed);
    }
    gamelog("Game action: Asteroid rotated.");
}

void Asteroid::getDmg(double dmg) {
    if (!_condition) return;

    _condition |= DAMAGED;
    math::Math::scale_shape(1.0 - _delta_scale / _scale, _collision_polygon);
    _scale -= _delta_scale;
    _life_count--;

    gamelog("Game action: Asteroid got damage");
    if (dmg == MAX_DMG || _life_count <= 0) {
        Player::addScore(40);
        _condition &= DEAD;
        gamelog("Game action: Asteroid died");
    } else {
        Player::addScore(10);
    }
}

void Asteroid::giveDmg(Hittable &object) const {
    object.getDmg(MAX_DMG);
}

void Asteroid::save(const pugi::xml_node_iterator _root) const {
    if (_root->child("Asteroids").empty()) {
        _root->append_child("Asteroids");
    }
    _root->child("Asteroids").append_child("Asteroid");
    Object::save(_root->child("Asteroids").child("Asteroid"));
    _root->child("Properties").child("State").append_child("Damage").set_value(
          std::to_string(_life_count).c_str());
}
