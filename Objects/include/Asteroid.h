//! @file
//! \brief Файл с классом астороида
//===========================================================================================================

#ifndef GAME_ASTEROID_H
#define GAME_ASTEROID_H

#include "Object.h"
#include "Player.h"

#define ASTEROID_ANGLE 1

//-----------------------------------------------------------------------------------------------------------
//! \class Asteroid
//! \brief Класс астероида
//-----------------------------------------------------------------------------------------------------------

class Asteroid : public Object {
 public:
//-----------------------------------------------------------------------------------------------------------
//! \brief Конструктор из фалов загрузки
//!
//! \param _root корень объекта в xml файле
//-----------------------------------------------------------------------------------------------------------

    Asteroid(const pugi::xml_node_iterator _root);

//-----------------------------------------------------------------------------------------------------------
//! \brief Конструктор по параметрам
//!
//! \param coords       координаты астероида
//! \param angle        угол от оси y
//! \param scale        масштаб
//! \param speed        скорость вращенияы
//! \param life_count   число допустимых поподаний, до уничтожения астероида
//! \param texture      id текстуры
//! \param shape        многоугольник, являющийся границами объекта
//-----------------------------------------------------------------------------------------------------------

    Asteroid(math::Coords coords, double angle, double scale, double speed, int life_count,
             size_t texture, sf::CircleShape shape);

//-----------------------------------------------------------------------------------------------------------
//! \brief Перемещение объекта
//!
//! \param dir параметр направления (может не использоваться)
//-----------------------------------------------------------------------------------------------------------

    void move(enum move_dir dir) override;

//-----------------------------------------------------------------------------------------------------------
//! \brief Функция получения урона
//!
//! \param dmg урон, который получает астероид
//-----------------------------------------------------------------------------------------------------------

    void getDmg(double dmg) override;

//-----------------------------------------------------------------------------------------------------------
//! \brief Функция получения урона
//! Астероид уничтожает любой объект с первого столкновения
//!
//! \param object объект, которому наносят урон
//-----------------------------------------------------------------------------------------------------------

    void giveDmg(Hittable &object) const override;

//-----------------------------------------------------------------------------------------------------------
//! \brief Сохраняет параметры астероида в файл
//!
//! \param _root корень xml дерева с объектами
//-----------------------------------------------------------------------------------------------------------

    void save(const pugi::xml_node_iterator _root) const override;

 private:
    double _delta_scale;    ///< размер уменьшения пули за один удар
    double _life_count;     ///< число допустимых поподаний, до уничтожения астероида
};


#endif //GAME_ASTEROID_H
