//! @file

#include "Logger.h"

namespace debug {
std::chrono::steady_clock::time_point Logger::start_log = std::chrono::steady_clock::now();

void Logger::game_log(const STRING_TYPE str_log) {
    std::ofstream log("game.log", std::ios_base::app);

    if (log.fail()) {
        errlog("Error open game log file");
        return;
    }
    auto hours = std::chrono::duration_cast<std::chrono::hours>(
          std::chrono::steady_clock::now() - start_log);
    auto minutes = std::chrono::duration_cast<std::chrono::minutes>(
          std::chrono::steady_clock::now() - start_log)
                   - std::chrono::duration_cast<std::chrono::minutes>(hours);
    auto seconds = std::chrono::duration_cast<std::chrono::seconds>(
          std::chrono::steady_clock::now() - start_log)
                   - std::chrono::duration_cast<std::chrono::seconds>(hours);

    log << "# " << hours.count() << ":" << minutes.count() << ":" << seconds.count();

    log << " #Game action : " << str_log << "\n";
    log.close();
}

void Logger::err_log(const STRING_TYPE str_log, int line, const char *file) {
    std::ofstream log("error.log", std::ios_base::app);

    if (log.fail()) {
        perror("Error open game log file");
        return;
    }

    auto hours = std::chrono::duration_cast<std::chrono::hours>(
          std::chrono::steady_clock::now() - start_log);
    auto minutes = std::chrono::duration_cast<std::chrono::minutes>(
          std::chrono::steady_clock::now() - start_log)
                   - std::chrono::duration_cast<std::chrono::minutes>(hours);
    auto seconds = std::chrono::duration_cast<std::chrono::seconds>(
          std::chrono::steady_clock::now() - start_log)
                   - std::chrono::duration_cast<std::chrono::seconds>(hours);

    log << "# " << hours.count() << ":" << minutes.count() << ":" << seconds.count();

    log << " #Got error on " << line << " lines in " << file << " : " << str_log << "\n";
    log.close();
}

void Logger::clear_game_log() {
    std::ofstream log("game.log");
    log.clear();
    log.close();
}

void Logger::clear_error_log() {
    std::ofstream log("error.log");
    log.clear();
    log.close();
}
}
