//! @file
//! \brief Исключения
//===========================================================================================================

#ifndef GAME_EXCEPTIONS_H
#define GAME_EXCEPTIONS_H

#include <utility>
#include "Logger.h"

//! Простраство имён для отладочных модулей
namespace debug {

//-----------------------------------------------------------------------------------------------------------------
//! \class Exception
//! \brief Общий класс исключений
//-----------------------------------------------------------------------------------------------------------------

class Exception: public std::exception {
 public:
    explicit Exception(STRING_TYPE msg)
          : m_msg(std::move(msg)) {}

    [[nodiscard]] const char* what() const noexcept override {
        return m_msg.c_str();
    }

 private:
    std::string m_msg;

};

//-----------------------------------------------------------------------------------------------------------
//! \class DivisionOnZero
//! \brief Исключения деления на ноль
//-----------------------------------------------------------------------------------------------------------

class DivisionOnZero: public Exception {
 public:
    DivisionOnZero() = delete;

    DivisionOnZero(STRING_TYPE err = "Runtime error: Division on zero")
          : Exception("Division on zero") {
        errlog(std::move(err));
    }
};

//-----------------------------------------------------------------------------------------------------------
//! \class InhibitedArgument
//! \brief Исключение запрещённого аргумента
//-----------------------------------------------------------------------------------------------------------

class InhibitedArgument: public Exception {
 public:
    InhibitedArgument() = delete;

    InhibitedArgument(STRING_TYPE err = "Runtime error: Inhibited argument")
          : Exception("Inhibited argument") {
        errlog(std::move(err));
    }
};
}

#endif //GAME_EXCEPTIONS_H
