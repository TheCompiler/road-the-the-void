//! @file
//! \brief Main игры
//! @mainpage
//! Игра про полёт в космосе.
//! Цель игры убить всех противников.
//! Игра имеет достаточно число багов и не точностей.
//!
//! \author Команда <b>"Мы"</b>:
//!         <p> TheCompiler         -- <i> 1 курс ИУ5 МГТУ им. Баумана </i> </p>
//!         <p> Александр Костарев  -- <i> 1 курс СГН3 МГТУ им. Баумана </i> </p>
//!         <p> Александр Егоров    -- <i> 1 курс СГН3 МГТУ им. Баумана </i> </p>
//! \date december 2019
//! \version alpha 1.0
//===========================================================================================================

#include "graphics.h"

int main() {
    Game game{};
    float CurrentFrame = 0;
    sf::Clock clock;
    sf::Clock menu_clock;
    sf::Int64 menu_time = menu_clock.getElapsedTime().asSeconds();
    while (game.is_open()) {
        float time = clock.getElapsedTime().asMicroseconds();
        clock.restart();
        time = time / TIME;

        game.check_key_event();

        if (game.check_key("escape")) {
            if (menu_clock.getElapsedTime().asSeconds() - menu_time > 1.2) {
                menu_time = menu_clock.getElapsedTime().asSeconds();
            }
        }
        (game.*game.get_action())();
    }
    return 0;
}
