#include <SFML/Graphics/CircleShape.hpp>
#include "Math.h"

namespace math {
inline bool Math::is_null(double a) {
    return fabs(a) < std::numeric_limits<double>::epsilon();
}

std::pair<double, double> Math::quad(double a, double b, double c) {
    std::pair<double, double> sol(0, 0);

    if (is_null(a)) {
        if (is_null(b)) {
            if (is_null(c)) {
                return {NULL, 1};
            }
            return {NULL, NULL};
        }

        sol.first = -c / b;
        sol.second = sol.first;
        return sol;
    }

    const double discr = b * b - 4 * a * c;
    if (discr < 0) {
        return {NULL, NULL};
    }

    sol.first = sqrt(discr);
    sol.second = (-b + sol.first) / (2 * a);
    sol.first = (-b - sol.first) / (2 * a);

    if (discr == 0 || (is_null((b)) && is_null(c))) {
        return sol;
    }
    return sol;
}

Alg_vec2 Math::anticipation(Coords target, Alg_vec2 dir_target,
                            Coords shooter, double speed_bullet) {
    double tx = target.x - shooter.x;
    double ty = target.y - shooter.y;
    double tvx = dir_target.x();
    double tvy = dir_target.y();

    double a = sq(tx) + sq(ty) - sq(speed_bullet);
    double b = 2 * (tvx * tx + tvy * ty);
    double c = sq(tx) + sq(ty);

    std::pair<double, double> ts = quad(a, b, c);

    Alg_vec2 sol = Alg_vec2(shooter, target);
    if (ts.first != (double) NULL) {
        double t = std::min(ts.first, ts.second);

        if (t < 0) {
            t = std::max(ts.first, ts.second);
        }
        if (t > 0) {
            sol = Alg_vec2(target.x + dir_target.x() * t,
                           target.y + dir_target.y() * t);
        }
    }
    return sol;
}

bool Math::collinearity(Alg_vec2 vec1, Alg_vec2 vec2) {
    if (vec1.x() == 0 && vec2.x() == 0) return true;
    if (vec1.y() == 0 && vec2.y() == 0) return true;
    if (vec2.x() == 0 || vec2.y() == 0) return false;

    return is_null(vec1.x() / vec2.x() - vec1.y() / vec2.y());
}

int Math::colide(const sf::Shape &moving, Alg_vec2 dir_moving,
                 const sf::Shape &obstacle) {
    Alg_vec2 axis_vec = dir_moving.normal_vec();

    Min_max proj_moving = shape_projection(moving, axis_vec);
    Min_max proj_obstacle = shape_projection(obstacle, axis_vec);

    auto delta_offset = Pair_double(Vector2d(moving.getPosition()
                                             - obstacle.getPosition()));

    double offset = Alg_vec2(delta_offset.x,
                             delta_offset.y).projection_on(axis_vec);

    proj_moving.min += offset;
    proj_moving.max += offset;

    if ((proj_obstacle.min - proj_moving.max) > 0
        || (proj_moving.min - proj_obstacle.max) > 0) {
        return 0;
    }

    if ((proj_obstacle.min - proj_moving.max) > 0) {
        return 1;
    }

    return -1;
}

bool Math::intersects(const sf::Shape &shape1, const sf::Shape &shape2) {
    int n_vertex = shape1.getPointCount();

    for (size_t j = n_vertex - 1, i = 0; i < n_vertex; j = i, ++i) {
        if (colide(shape1, Alg_vec2(Coords((Vector2d) shape1.getPoint(j)),
                                    Coords((Vector2d) shape1.getPoint(i))), shape2) != 0) {
            return true;
        }
    }

    n_vertex = shape2.getPointCount();

    for (size_t j = n_vertex - 1, i = 0; i < n_vertex; j = i, ++i) {
        if (colide(shape1, Alg_vec2(Coords((Vector2d) shape2.getPoint(j)),
                                    Coords((Vector2d) shape2.getPoint(i))), shape2) != 0) {
            return true;
        }
    }

    return false;
}

Math::Min_max Math::shape_projection(const sf::Shape &shape, Alg_vec2 axis) {
    size_t n_vertex = shape.getPointCount();

    double min = std::numeric_limits<double>::max();
    double max = std::numeric_limits<double>::min();

    for (size_t i = 0; i < n_vertex; ++i) {
        auto vertex = shape.getPoint(i);
        auto vec = Alg_vec2(vertex.x, vertex.y);

        double proj = vec.projection_on(axis);
        min = std::min(min, proj);
        max = std::max(max, proj);
    }

    return {min, max};
}

double Math::to_degrees(double radian) {
    return radian * 180.0 / M_PI;
}

double Math::to_radian(double degrees) {
    return degrees * M_PI / 180.0;
}

int8_t Math::sign(double value) {
    if (value < 0) {
        return -1;
    } else {
        return 1;
    }
}

bool Math::right_triple_vec(Alg_vec3 vec1, Alg_vec3 vec2, Alg_vec3 vec3) {
    double main_det = vec1.x() * vec2.y() * vec3.z() + vec1.y() * vec2.z() * vec3.x() +
                      vec1.z() * vec2.x() * vec3.y();
    double side_det = vec1.z() * vec2.y() * vec3.x() + vec1.x() * vec2.z() * vec3.y() +
                      vec1.y() * vec2.x() * vec3.z();
    if (main_det - side_det > 0) {
        return true;
    }
    return false;
}

double Math::shortest_rotate_vec(Alg_vec3 vec1, Alg_vec3 vec2) {
    double angle = vec1 ^vec2;
    Alg_vec3 normal_vec(0, 0, 1);
    if (right_triple_vec(vec1, vec2, normal_vec)) {
        return -1 * angle;
    }
    return angle;
}

void Math::scale_shape(double new_scale, sf::CircleShape &shp) {
    shp.setRadius(shp.getRadius() * new_scale);
    shp.setOrigin(shp.getRadius(), shp.getRadius());
}
}