#include "Math.h"
#include "Alg_vec3.h"

namespace math {
Alg_vec3::Alg_vec3(double x, double y, double z)
      : _x(x)
        , _y(y)
        , _z(z) {}

Alg_vec3::Alg_vec3(Coords v1, Coords v2)
      : _x(v2.x - v1.x)
        , _y(v2.y - v1.y)
        , _z(v2.z - v1.z) {}

double Alg_vec3::x() const {
    return _x;
}

double Alg_vec3::y() const {
    return _y;
}

double Alg_vec3::z() const {
    return _z;
}

bool Alg_vec3::is_zero() const {
    return Math::is_null(_x) && Math::is_null(_y) && Math::is_null(_z);
}

double Alg_vec3::len() const {
    return sqrt(sq(_x) + sq(_y) + sq(_z));
}

double Alg_vec3::squared_len() const {
    return sq(_x) + sq(_y) + sq(_z);
}

double Alg_vec3::operator^(Alg_vec3 vec) const {
    if (is_zero() || vec.is_zero()) {
        errlog("Problem: Zero len of vector");
        return NAN;
    }

    double angle = acos(fabs((*this) * vec) / (len() * vec.len()));
    if (std::isnan(angle)) {
        return 0;
    }
    return angle;
}

double Alg_vec3::operator*(Alg_vec3 vec) const {
    return _x * vec._x + _y * vec._y + _z * vec._z;
}

Alg_vec3 Alg_vec3::operator*(double k) const {
    return Alg_vec3(_x * k, _y * k, _z * k);
}

Alg_vec3 operator*(double k, const Alg_vec3 &vec) {
    return Alg_vec3(vec._x * k, vec._y * k, vec._z * k);
}

Alg_vec3 Alg_vec3::operator/(double k) const {
    if (k == 0) {
        errlog("Problem: Division on zero");
        throw debug::DivisionOnZero("Runtime error: Division on zero");
    }
    return Alg_vec3(_x / k, _y / k, z() / k);
}

Alg_vec3::operator Alg_vec2() {
    return Alg_vec2(_x, _y);
}
}