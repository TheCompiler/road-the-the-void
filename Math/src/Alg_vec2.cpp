#include "Math.h"
#include "Alg_vec2.h"

namespace math {
Coords::Coords(double x, double y, double z)
      : x(x)
        , y(y)
        , z(z) {}

Coords::Coords(Vector2d sfml)
      : x(sfml.x)
        , y(sfml.y)
        , z(0) {}

Coords::Coords(Vector3d sfml)
      : x(sfml.x)
        , y(sfml.y)
        , z(sfml.z) {}

bool Coords::operator==(const Coords &coord) {
    return Math::is_null(coord.x - x) && Math::is_null(coord.y - y)
           && Math::is_null(coord.z - z);
}

Coords &Coords::operator+(double offset) {
    x += offset;
    y += offset;
    return *this;
}

Alg_vec2::Alg_vec2(double x, double y)
      : _x(x)
        , _y(y) {}

Alg_vec2::Alg_vec2(double angle)
      : _x(sin(angle))
        , _y(cos(angle)) {}

Alg_vec2::Alg_vec2(Coords v1, Coords v2)
      : _x(v2.x - v1.x)
        , _y(v2.y - v1.y) {}

double Alg_vec2::x() const {
    return _x;
}

double Alg_vec2::y() const {
    return _y;
}

bool Alg_vec2::is_zero() const {
    return Math::is_null(_x) && Math::is_null(_y);
}

double Alg_vec2::len() const {
    return sqrt(sq(_x) + sq(_y));
}

double Alg_vec2::squared_len() const {
    return sq(_x) + sq(_y);
}

double Alg_vec2::projection_on(const Alg_vec2 &axis) const {
    if (Math::is_null(axis.squared_len())) {
        errlog("Problem: Zero axis vector");
        throw debug::DivisionOnZero("Runtime error: Zero axis vector");
    }

    return ((*this) * axis) / axis.len();
}

Alg_vec2 Alg_vec2::normal_vec() const {
    return Alg_vec2(-_y, _x);
}

double Alg_vec2::operator^(Alg_vec2 vec) const {
    if (is_zero() || vec.is_zero()) {
        errlog("Problem: Zero len of vector");
        return NAN;
    }

    double angle = acos(fabs((*this) * vec) / (len() * vec.len()));
    if (std::isnan(angle)) {
        return 0;
    }
    return angle;
}

double Alg_vec2::operator*(Alg_vec2 vec) const {
    return _x * vec._x + _y * vec._y;
}

Alg_vec2 Alg_vec2::operator*(double k) const {
    return Alg_vec2(_x * k, _y * k);
}

Alg_vec2 operator*(double k, const Alg_vec2 &vec) {
    return Alg_vec2(vec._x * k, vec._y * k);
}

Alg_vec2 Alg_vec2::operator/(double k) const {
    if (k == 0) {
        errlog("Problem: Division on zero");
        throw debug::DivisionOnZero("Runtime error: Division on zero");
    }
    return Alg_vec2(_x / k, _y / k);
}

Alg_vec2::operator Alg_vec3() {
    return Alg_vec3(_x, _y, 0);
}
}
