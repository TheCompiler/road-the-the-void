var searchData=
[
  ['object_265',['Object',['../class_object.html#afe9eeddd7068a37f62d3276a2fb49864',1,'Object::Object()=default'],['../class_object.html#a0d1fe16d035e5d0db5bb3b1763024943',1,'Object::Object(const pugi::xml_node_iterator _root)'],['../class_object.html#a3355949c4e9d65fb6403477836067b2f',1,'Object::Object(double x, double y, double angle, double scale, life_length length, size_t texture, sf::CircleShape shape)'],['../class_object.html#a806c72ea9da12830f3b879dba57346cc',1,'Object::Object(math::Coords coords, double angle, double scale, double speed, life_length length, size_t texture, sf::CircleShape shape)']]],
  ['onkey_5fdown_266',['OnKey_Down',['../class_game.html#a0a9be4e4e42ed6508de1763551204763',1,'Game']]],
  ['onkey_5fup_267',['OnKey_Up',['../class_game.html#a467915aaf14246b7d2cacb57d5a0c25b',1,'Game']]],
  ['operator_20alg_5fvec2_268',['operator Alg_vec2',['../classmath_1_1_alg__vec3.html#a24d511375b468bdedbdff72879af8014',1,'math::Alg_vec3']]],
  ['operator_20alg_5fvec3_269',['operator Alg_vec3',['../classmath_1_1_alg__vec2.html#ae3db90ae8be0c84ebdfe5c7efbc7cb0e',1,'math::Alg_vec2']]],
  ['operator_2a_270',['operator*',['../classmath_1_1_alg__vec2.html#a88da8b828ad8edeea2f0ea988bb82fac',1,'math::Alg_vec2::operator*(Alg_vec2 vec) const'],['../classmath_1_1_alg__vec2.html#ac92d541763cfc6ffe7bb1cba56d7c709',1,'math::Alg_vec2::operator*(double k) const'],['../classmath_1_1_alg__vec3.html#a31c106c1984a9f3e41590a8f912d8ad8',1,'math::Alg_vec3::operator*(Alg_vec3 vec) const'],['../classmath_1_1_alg__vec3.html#ae50f411e026c67461dd12f0893b5b466',1,'math::Alg_vec3::operator*(double k) const'],['../namespacemath.html#a3af95a2afa19a77c9ed526abd466c636',1,'math::operator*(double k, const Alg_vec2 &amp;vec)'],['../namespacemath.html#ac7767b91a8761b74eb941992ec807990',1,'math::operator*(double k, const Alg_vec3 &amp;vec)']]],
  ['operator_2b_271',['operator+',['../structmath_1_1_coords.html#a21a6c9268cabe0df01bfcba90aeef760',1,'math::Coords']]],
  ['operator_2f_272',['operator/',['../classmath_1_1_alg__vec2.html#a2176e29f2b4fde49370bd1b37645fb0e',1,'math::Alg_vec2::operator/()'],['../classmath_1_1_alg__vec3.html#af4d875fe1bd36312a357dd0d71b564cd',1,'math::Alg_vec3::operator/()']]],
  ['operator_3d_3d_273',['operator==',['../structmath_1_1_coords.html#aecbf344f6830b50e512aaa4d1df0c0a8',1,'math::Coords']]],
  ['operator_5e_274',['operator^',['../classmath_1_1_alg__vec2.html#a703084ff050e99246a16db2ddc102ef2',1,'math::Alg_vec2::operator^()'],['../classmath_1_1_alg__vec3.html#a0d84db3a75b28cf3a3b6c92f3b0a6da4',1,'math::Alg_vec3::operator^()']]]
];
