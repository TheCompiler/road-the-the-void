var indexSectionsWithContent =
{
  0: "_abcdeghilmnopqrstuvwxyz~",
  1: "abcdeghilmoprstw",
  2: "dm",
  3: "abceglmopstvw",
  4: "abcdegilmnopqrstuwxyz~",
  5: "_g",
  6: "clm",
  7: "adlms",
  8: "o",
  9: "adegm"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "enums",
  7: "enumvalues",
  8: "related",
  9: "defines"
};

var indexSectionLabels =
{
  0: "Указатель",
  1: "Классы",
  2: "Пространства имен",
  3: "Файлы",
  4: "Функции",
  5: "Переменные",
  6: "Перечисления",
  7: "Элементы перечислений",
  8: "Друзья",
  9: "Макросы"
};

