var searchData=
[
  ['addobject_206',['addObject',['../class_scene_t.html#a30e709101bb1b14f1d14a791cbbe6df9',1,'SceneT']]],
  ['addscore_207',['addScore',['../class_player.html#a214ae30e18e4b9a4a87a025e486f8972',1,'Player']]],
  ['alg_5fvec2_208',['Alg_vec2',['../classmath_1_1_alg__vec2.html#aeb1185dad7289df965a65a5555fb1c42',1,'math::Alg_vec2::Alg_vec2()=default'],['../classmath_1_1_alg__vec2.html#ae9d537b00539bd0fb814bc15fc6a79ef',1,'math::Alg_vec2::Alg_vec2(double x, double y)'],['../classmath_1_1_alg__vec2.html#a136b753126f292da968f40a04990846d',1,'math::Alg_vec2::Alg_vec2(double angle)'],['../classmath_1_1_alg__vec2.html#a5e7a52ef2c18b2dea0d71c63e1a808fe',1,'math::Alg_vec2::Alg_vec2(Coords v1, Coords v2)']]],
  ['alg_5fvec3_209',['Alg_vec3',['../classmath_1_1_alg__vec3.html#a4faa96df5a2c5604160bbce98ff5854b',1,'math::Alg_vec3::Alg_vec3()=default'],['../classmath_1_1_alg__vec3.html#a66945920ffe20436c23501d9d2069435',1,'math::Alg_vec3::Alg_vec3(double x, double y, double z)'],['../classmath_1_1_alg__vec3.html#ad6f684f81f70018a3df47dbe33e7a8d4',1,'math::Alg_vec3::Alg_vec3(Coords v1, Coords v2)']]],
  ['anticipation_210',['anticipation',['../classmath_1_1_math.html#ae9410249acfc277fabb6e512a2399712',1,'math::Math']]],
  ['asteroid_211',['Asteroid',['../class_asteroid.html#acbdf14d8e69cef3bbcdfaa403e8ee38b',1,'Asteroid::Asteroid(const pugi::xml_node_iterator _root)'],['../class_asteroid.html#abb0ce4ec6a110aa5a5f76c8de78982c2',1,'Asteroid::Asteroid(math::Coords coords, double angle, double scale, double speed, int life_count, size_t texture, sf::CircleShape shape)']]]
];
