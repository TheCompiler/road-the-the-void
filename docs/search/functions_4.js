var searchData=
[
  ['enemy_232',['Enemy',['../class_enemy.html#a59634e3504b1234afadf9df5a1044a78',1,'Enemy::Enemy()=default'],['../class_enemy.html#aa82881cca2f2c4b2655ddb10f7df9755',1,'Enemy::Enemy(const pugi::xml_node_iterator _root, SceneT *scene)'],['../class_enemy.html#a923052050279d8fe0c45b76d97c6d899',1,'Enemy::Enemy(double x, double y, double angle, double scale, double hp, double dmg, double acceleration, double max_speed, SceneT &amp;scene, size_t texture, sf::CircleShape shape)']]],
  ['err_5flog_233',['err_log',['../classdebug_1_1_logger.html#a8204c86eda9b833eaa7c89285deccec6',1,'debug::Logger']]]
];
