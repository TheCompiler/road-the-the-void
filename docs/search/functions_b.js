var searchData=
[
  ['player_275',['Player',['../class_player.html#aaa23b3bf80e8c0267cf08d4fe4d6ddc1',1,'Player::Player()=default'],['../class_player.html#ad7fadd6d57f057805e8f70121bd6c55e',1,'Player::Player(const pugi::xml_node_iterator _root)'],['../class_player.html#a87d56c93c08c2753fd22799d48fed449',1,'Player::Player(double x, double y, double angle, double scale, double hp, double dmg, double acceleration, double max_speed, size_t texture, sf::CircleShape shape)']]],
  ['projection_5fon_276',['projection_on',['../classmath_1_1_alg__vec2.html#aecc6deeaaeb635bd602bb87366d437fc',1,'math::Alg_vec2']]]
];
