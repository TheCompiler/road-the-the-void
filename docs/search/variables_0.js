var searchData=
[
  ['_5fangle_304',['_angle',['../class_object.html#afec7260fb48232d8872807bf33a02743',1,'Object']]],
  ['_5fangle_5fspeed_305',['_angle_speed',['../class_object.html#a4e61cc9e34ff17e643ab4b187a54f590',1,'Object']]],
  ['_5fcollision_5fpolygon_306',['_collision_polygon',['../class_object.html#a5c8a581d5bf4027eda6d9e477c710abc',1,'Object']]],
  ['_5fcondition_307',['_condition',['../class_object.html#af4fa2c64097d63c0af2a672d747eb71d',1,'Object']]],
  ['_5fcoords_308',['_coords',['../class_object.html#a045d7fbb6029fa60c1ef33a887333314',1,'Object']]],
  ['_5fdmg_309',['_dmg',['../class_weapon.html#a7dd29b48361af593056ef440e25790d4',1,'Weapon']]],
  ['_5fid_5ftexture_310',['_id_texture',['../class_object.html#a09df8e6d97faf07a21a1ae26cd8b2fa4',1,'Object']]],
  ['_5fid_5ftexture_5fbullet_311',['_id_texture_bullet',['../class_weapon.html#ac2215a1b8b82f78e20105605f9bb719a',1,'Weapon']]],
  ['_5flife_5flen_312',['_life_len',['../class_object.html#afa28952724467f83ce21641716a2842f',1,'Object']]],
  ['_5fradius_5fbullet_313',['_radius_bullet',['../class_weapon.html#aa3544e384fe7466d798a12295add4e04',1,'Weapon']]],
  ['_5fscale_314',['_scale',['../class_object.html#af53db00d79b5b6b02935abb05e48f11b',1,'Object']]],
  ['_5fspeed_315',['_speed',['../class_object.html#ad33c5edbccaaba8b39b6e36cfd6fc4f7',1,'Object']]],
  ['_5fspeed_5fbullet_316',['_speed_bullet',['../class_weapon.html#aee247d00495cadeaadb21bff52b8dcae',1,'Weapon']]],
  ['_5ftextures_317',['_textures',['../class_textures.html#a499fde8be8601e0fbdd8e4f15b78dd3e',1,'Textures']]],
  ['_5fweapons_318',['_weapons',['../class_weapons.html#a9185dc600d02c1ced5e903a566886a4f',1,'Weapons']]]
];
