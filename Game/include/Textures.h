//! @file
//! \brief Файл с загруженными текстурами
//===========================================================================================================

#ifndef GAME_TEXTURES_H
#define GAME_TEXTURES_H

#include <vector>
#include <SFML/Graphics/Texture.hpp>
#include "Config.h"

//-----------------------------------------------------------------------------------------------------------
//! \class Textures
//! \brief Статический класс загруженных текстур
//-----------------------------------------------------------------------------------------------------------

class Textures {
 public:

    static std::vector<sf::Texture> _textures; ///< загруженные текстуры

//-----------------------------------------------------------------------------------------------------------
//! \brief Конструктор по-умолчанию
//-----------------------------------------------------------------------------------------------------------

    Textures();

//-----------------------------------------------------------------------------------------------------------
//! \brief Конструктор создания из файла
//!
//! \param file_name имя файла
//-----------------------------------------------------------------------------------------------------------

    explicit Textures(std::string file_name);
};

#endif //GAME_TEXTURES_H
