#include "Textures.h"

std::vector<sf::Texture> Textures::_textures = {};

Textures::Textures() {
    _textures.resize(10);
    _textures[0].loadFromFile(DIRECTORY_OF_TEXTURE + "bg.jpg");
    _textures[1].loadFromFile(DIRECTORY_OF_TEXTURE + "ship.png");
    _textures[2].loadFromFile(DIRECTORY_OF_TEXTURE + "asteroid.png");
    _textures[3].loadFromFile(DIRECTORY_OF_TEXTURE + "enemy.png");
    _textures[4].loadFromFile(DIRECTORY_OF_TEXTURE + "fireball_enemy.png");
    _textures[5].loadFromFile(DIRECTORY_OF_TEXTURE + "fireball_ship.png");
    _textures[6].loadFromFile(DIRECTORY_OF_TEXTURE + "menu.jpg");
    _textures[7].loadFromFile(DIRECTORY_OF_TEXTURE + "start.png");
    _textures[8].loadFromFile(DIRECTORY_OF_TEXTURE + "resume.png");
    _textures[9].loadFromFile(DIRECTORY_OF_TEXTURE + "exit.png");
}

Textures::Textures(std::string file_name) {

}
