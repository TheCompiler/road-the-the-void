#include "Weapons.h"

std::map<std::string, Weapon> Weapons::_weapons = {};

Weapons::Weapons(std::string file_name) {
    pugi::xml_document doc;
    if (!doc.load_file(file_name.c_str())) return;

    pugi::xml_node weapons = doc.child("Game").child("Weapons");

    for (pugi::xml_node_iterator it = weapons.begin(); it != weapons.end(); ++it) {
        _weapons[it->child("Name").text().as_string()] = Weapon(it);
    }
}

void Weapons::save(const pugi::xml_node_iterator _root) {
    _root->append_child("Weapons");
    for (auto &i : _weapons) {
        _root->child("Weapons").append_child("Weapon").append_child("Name").set_value(
              i.first.c_str());
        i.second.save(_root->child("Weapons").child("Weapon"));
    }
}
