#include "graphics.h"
#include <math.h>
#include <chrono>

Game::Game()
      : _scene()
        , _window(sf::VideoMode(SCREEN_RESOLUTION_X, SCREEN_RESOLUTION_Y), "Road to the Void")
        , _calls()
        , left_click(0) {
    Textures();
    Weapons("xgconsole.xml");

    _calls.push(&Game::game_menu);
    sf::Texture *menu_texture = new sf::Texture();
    menu_texture->loadFromFile("Game/img/menu.jpg");
    _menu.setTexture(*menu_texture);
    _menu.setScale((SCREEN_RESOLUTION_X / X_BACKGROUND),
                   (SCREEN_RESOLUTION_Y / Y_BACKGROUND));

    rev_keys[sf::Keyboard::Escape] = "escape";

    rev_keys[sf::Keyboard::Left] = "turn_left";
    rev_keys[sf::Keyboard::Right] = "turn_right";
    rev_keys[sf::Keyboard::Down] = "down";
    rev_keys[sf::Keyboard::Up] = "up";

    rev_keys[sf::Keyboard::A] = "turn_left";
    rev_keys[sf::Keyboard::D] = "turn_right";
    rev_keys[sf::Keyboard::S] = "down";
    rev_keys[sf::Keyboard::W] = "up";

    rev_keys[sf::Keyboard::Space] = "shot";
}

void Game::OnKey_Up(sf::Event &e) {
    if (rev_keys.find(e.key.code) != rev_keys.end()) {
        keys[rev_keys[e.key.code]] = false;
    }
}

bool Game::check_key(std::string key) {
    return keys[key];
}

void Game::OnKey_Down(sf::Event &e) {
    if (rev_keys.find(e.key.code) != rev_keys.end()) {
        keys[rev_keys[e.key.code]] = true;
    }
}

bool Game::is_open() const {
    return _window.isOpen();
}

void Game::close() {
    _scene.clear_level();
    _window.close();
}

void Game::game_view() {
    if (keys["escape"]) {
        keys["escape"] = false;
        _calls.push(&Game::game_pause);
    }


    if (!keys["shot"]) {
        shot = true;
    }
    if ((keys["shot"]) && (shot)) {
        _scene.getPlayer().shot(_scene);
        shot = false;
    }
    if (keys["turn_left"]) {
        _scene.getPlayer().move(MOVE_LEFT);
    }

    if (keys["up"]) {
        _scene.getPlayer().move(MOVE_UP);
    }

    if (keys["turn_right"]) {
        _scene.getPlayer().move(MOVE_RIGHT);
    }

    if (keys["down"]) {
        _scene.getPlayer().move(MOVE_DOWN);
    }
}

void Game::game_pause() {
    _window.clear();

    _window.setView(_window.getDefaultView());   //
    _window.draw(_menu);

    sf::Sprite resume;
    resume.setTexture(Textures::_textures[8]);
    //  resume.setOrigin(resume.getPosition().x / 2, resume.getPosition().y / 2);
    resume.setPosition(600, 200);
    resume.setColor(sf::Color::Color::White);

    sf::Sprite t_exit;
    t_exit.setTexture(Textures::_textures[9]);
    t_exit.setPosition(700, 400);
    t_exit.setColor(sf::Color::Color::White);
    _window.draw(t_exit);

    if (sf::IntRect(resume.getPosition().x, resume.getPosition().y,
                    resume.getTextureRect().width, resume.getTextureRect().height).contains(
          sf::Mouse::getPosition(_window))) {
        resume.setColor(sf::Color::Color::Blue);

        if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
            _calls.pop();
        }
    }
    _window.draw(resume);


    if (sf::IntRect(t_exit.getPosition().x, t_exit.getPosition().y,
                    t_exit.getTextureRect().width, t_exit.getTextureRect().height).contains(
          sf::Mouse::getPosition(_window))) {
        t_exit.setColor(sf::Color::Color::Blue);
        if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
            left_click = true;
            _calls.pop();
            _calls.pop();
        }
    }
    _window.draw(t_exit);
    if (keys["escape"]) {
        keys["escape"] = false;
        _calls.pop();
    }

    _window.display();
}

void (Game::* const Game::get_action())() {
    return _calls.top();
}


void Game::game_menu() {
    _window.clear();
    _window.setView(_window.getDefaultView());   //

    _window.draw(_menu);

    sf::Sprite start_trip;
    start_trip.setTexture(Textures::_textures[7]);
    start_trip.setPosition(600, 200);
    start_trip.setColor(sf::Color::Color::White);

    sf::Sprite t_exit;
    t_exit.setTexture(Textures::_textures[9]);
    t_exit.setPosition(700, 400);
    t_exit.setColor(sf::Color::Color::White);


    if (sf::IntRect(start_trip.getPosition().x, start_trip.getPosition().y,
                    start_trip.getTextureRect().width, start_trip.getTextureRect().height).contains(
          sf::Mouse::getPosition(_window))) {
        start_trip.setColor(sf::Color::Color::Blue);

        if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
            _calls.push(&Game::draw_game);
            _scene.load_level("xgconsole.xml");
        }
    }
    _window.draw(start_trip);

    if (!sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
        {
            left_click = false;
        }
    }

    if (sf::IntRect(t_exit.getPosition().x, t_exit.getPosition().y,
                    t_exit.getTextureRect().width, t_exit.getTextureRect().height).contains(
          sf::Mouse::getPosition(_window))) {
        t_exit.setColor(sf::Color::Color::Red);
        if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
            if (!left_click) {
                _calls.pop();
                _scene.clear_level();
                close();
            }

        }
    }
    _window.draw(t_exit);
    _window.display();
}

void Game::draw_game() {
    game_view();
    _window.clear();
    _window.setFramerateLimit(60);

    update_camera();

    _scene.check_collide();

    if (!(_scene.getPlayer().getCondition() ^ DEAD)) {
        std::chrono::steady_clock::time_point time_win = std::chrono::steady_clock::now();

        while (std::chrono::duration_cast<std::chrono::seconds>(
              std::chrono::steady_clock::now() - time_win).count() < 2) {
            sf::Font font;
            font.loadFromFile("Game/font/font.ttf");
            sf::Text t_start("", font, 190);
            t_start.setColor(sf::Color::Red);
            //text.setStyle(sf::Text::Bold | sf::Text::Underlined);

            t_start.setString("YOU DIED!");
            t_start.setPosition(camera.getCenter().x - 600, camera.getCenter().y);
            _window.draw(t_start);
            _window.display();
        }
        _calls.pop();
        _scene.clear_level();
        return;
    }

    if (!Enemy::getNEnemyes()) { /// VICTORY!!!!!
        std::chrono::steady_clock::time_point time_win = std::chrono::steady_clock::now();

        while (std::chrono::duration_cast<std::chrono::seconds>(
              std::chrono::steady_clock::now() - time_win).count() < 3) {
            sf::Font font;
            font.loadFromFile("Game/font/font.ttf");
            sf::Text t_start("", font, 200);
            t_start.setColor(sf::Color::Green);
            //text.setStyle(sf::Text::Bold | sf::Text::Underlined);

            t_start.setString("WIN");
            t_start.setPosition(camera.getCenter().x, camera.getCenter().y);
            _window.draw(t_start);
            _window.display();
        }
        _calls.pop();
        _scene.clear_level();
        return;
    }

    _scene.draw_all_objects(_window);
    draw_player_info();

    clear_objects();
    _window.display();
}

void Game::draw_player_info() {
    sf::Font font;
    font.loadFromFile("Game/font/font.ttf");
    sf::Text t_start("", font, 40);
    t_start.setColor(sf::Color::Red);
    //text.setStyle(sf::Text::Bold | sf::Text::Underlined);

    t_start.setString(
          "HP : " + std::to_string((int) _scene.getPlayer().getHp()));
    t_start.setPosition(camera.getCenter().x + 450, camera.getCenter().y + 300);
    _window.draw(t_start);

    sf::Text score("", font, 40);
    score.setColor(sf::Color::Red);
    //text.setStyle(sf::Text::Bold | sf::Text::Underlined);

    score.setString(
          "Score : " + std::to_string(Player::getScore()));
    score.setPosition(camera.getCenter().x - 100, camera.getCenter().y - 350);
    _window.draw(score);
}

void Game::clear_objects() {
    int life_time_after_dead = this->life_clock.getElapsedTime().asSeconds();

    if ((life_time_after_dead % 3) == 0) {
        SceneT().delete_sl_objs();
    }
    if ((life_time_after_dead % 10) == 0) {
        SceneT().delete_ml_objs();
    }

    if ((life_time_after_dead % 40) == 0) {
        SceneT().delete_ll_objs();
    }
}

void Game::update_camera() {
    auto player = _scene.getPlayer();

    camera.reset(sf::FloatRect(222, 50, SCREEN_RESOLUTION_X / SCALE_FACTOR,
                               SCREEN_RESOLUTION_Y / SCALE_FACTOR));
    camera.setCenter(player.getCoords().x,
                     -player.getCoords().y);

    double camera_scale = 1 - (pow(0.5, SCALE_FACTOR));
    double x = player.getCoords().x;
    double y = player.getCoords().y;
    if (x >= X_BACKGROUND * camera_scale) {
        x = X_BACKGROUND * camera_scale;
    } else {
        if (x <= X_BACKGROUND - (X_BACKGROUND * camera_scale)) {
            x = X_BACKGROUND - (X_BACKGROUND * camera_scale);
        }
    }
    if (y >= Y_BACKGROUND * camera_scale) {
        y = Y_BACKGROUND * camera_scale;
    } else {
        if (y <= Y_BACKGROUND - (Y_BACKGROUND * camera_scale)) {
            y = Y_BACKGROUND - (Y_BACKGROUND * camera_scale);
        }
    }
    camera.setCenter(x, -y);
    _window.setView(camera);
}

void Game::check_key_event() {
    sf::Event event;
    while (_window.pollEvent(event)) {
        switch (event.type) {
            case sf::Event::KeyPressed:
                OnKey_Down(event);
                break;
            case sf::Event::KeyReleased:
                OnKey_Up(event);
                break;
            case sf::Event::Closed:
                close();
            default:
                break;
        }
    }
}
