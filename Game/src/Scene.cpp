#include "Scene.h"

SceneT::SceneT()
      : _player()
        , _bg()
        , _score(0)
        , long_life_objs()
        , medium_life_objs()
        , short_life_objs() {}

void SceneT::check_collide() {
    std::vector<std::shared_ptr<Object>> _objects = getObjects();

    for (size_t j = 0; j < _objects.size(); ++j) {
        if (!_objects[j]->getCondition()) continue;
        if (!_player.getCondition()) break;

        if (math::Alg_vec2(_player.getCoords(), _objects[j]->getCoords()).squared_len()
            < sq(_player.getShape().getRadius()) + sq(_objects[j]->getShape().getRadius())) {
            if (math::Math::intersects(_player.getShape(), _objects[j]->getShape())) {
                _player.giveDmg(*_objects[j]);
                _objects[j]->giveDmg(_player);
            }
        }
    }

    for (size_t i = 0; i < _objects.size(); ++i) {
        if (_objects[i]->getCondition() & MOVED) {
            for (size_t j = 0; j < _objects.size(); ++j) {
                if (_objects[j]->getCondition() & MOVED && j <= i) continue;
                if (!_objects[j]->getCondition()) continue;
                if (!_objects[i]->getCondition()) break;

                if (math::Alg_vec2(_objects[i]->getCoords(), _objects[j]->getCoords()).squared_len()
                    < sq(_objects[i]->getShape().getRadius()) +
                      sq(_objects[j]->getShape().getRadius())) {
                    if (math::Math::intersects(_objects[i]->getShape(), _objects[j]->getShape())) {
                        _objects[i]->giveDmg(*_objects[j]);
                        _objects[j]->giveDmg(*_objects[i]);
                    }
                }
            }
        }
    }
    _player.setCoords(std::min((double) _background.getTextureRect().width,
                               std::max(_player.getCoords().x, 0.0)),
                      std::min((double) _background.getTextureRect().height,
                               std::max(_player.getCoords().y, 0.0)));


    for (auto &_object : _objects) {
        if (_object->getCoords().x > _background.getTextureRect().width ||
            _object->getCoords().y > _background.getTextureRect().height) {
            _object->setCondition(DEAD);
        }
    }
}

const std::vector<std::shared_ptr<Object>> SceneT::getObjects() const {
    std::vector<std::shared_ptr<Object>> return_objects;
    for (auto it : long_life_objs) {
        return_objects.push_back(it);
    }

    for (auto it : medium_life_objs) {
        return_objects.push_back(it);
    }

    for (auto it : short_life_objs) {
        return_objects.push_back(it);
    }
    return return_objects;
}

void SceneT::addObject(std::shared_ptr<Object> object) {
    switch (object->getLifeLen()) {
        case LONG_LIFE:
            long_life_objs.push_back(object);
            break;
        case MEDIUM_LIFE:
            medium_life_objs.push_back(object);
            break;
        case SHORT_LIFE:
            short_life_objs.push_back(object);
            break;
    }
}

void SceneT::delete_ll_objs() {
    std::vector<std::shared_ptr<Object>> return_objects;
    for (const auto &it: long_life_objs) {
        if (it->getCondition())
            return_objects.push_back(it);
    }

    long_life_objs = return_objects;
}

void SceneT::delete_ml_objs() {
    std::vector<std::shared_ptr<Object>> return_objects;
    for (const auto &it: medium_life_objs) {
        if (it->getCondition())
            return_objects.push_back(it);
    }

    medium_life_objs = return_objects;
}

void SceneT::delete_sl_objs() {
    std::vector<std::shared_ptr<Object>> return_objects;
    for (const auto &it: short_life_objs) {
        if (it->getCondition())
            return_objects.push_back(it);
    }

    short_life_objs = return_objects;
}

Player &SceneT::getPlayer() {
    return _player;
}

void SceneT::setPlayer(Player player) {
    _player = player;
}

void SceneT::load_level(std::string xml_file) {
    clear_level();
    pugi::xml_document doc;
    if (!doc.load_file(xml_file.c_str())) return;

    pugi::xml_node enemies = doc.child("Game").child("Objects").child("Enemies");
    Enemy::setNEnemyes(enemies.attribute("count").as_ullong());

    pugi::xml_node asteroids = doc.child("Game").child("Objects").child("Asteroids");
    pugi::xml_node bullets = doc.child("Game").child("Objects").child("Bullets");
    pugi::xml_node player = doc.child("Game").child("Objects").child("Player");
    Player::setScore(doc.child("Game").child("Score").text().as_ullong());

    _player = Player(player);

    _background_file = doc.child("Game").attribute("Background").value();
    _bg.loadFromFile(_background_file);
    _background.setTexture(_bg);
    _background.setScale((SCREEN_RESOLUTION_X / X_BACKGROUND),
                         (SCREEN_RESOLUTION_Y / Y_BACKGROUND));
    _background.setPosition(0, -_background.getTextureRect().height);

    for (pugi::xml_node_iterator it = enemies.begin(); it != enemies.end(); ++it) {
        std::shared_ptr<Enemy> tmp(new Enemy(it, this));
        medium_life_objs.push_back(tmp);
    }
    for (pugi::xml_node_iterator it = asteroids.begin(); it != asteroids.end(); ++it) {
        std::shared_ptr<Asteroid> tmp(new Asteroid(it));
        medium_life_objs.push_back(tmp);
    }

    Player::setScore(0);
}

void SceneT::save_level(std::string file_name) const {
    pugi::xml_document doc;
    doc.load_string(((std::string) "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>"
                     + "<Game FormatVersion = \"1\"Background=\"" + _background_file + "\">" +
                     "</Game>").c_str());
    doc.append_child("Objects");
    doc.append_child("Score").set_value(std::to_string(Player::getScore()).c_str());

    _player.save(doc.child("Objects"));
    for (auto &i : medium_life_objs) {
        if (!i->getCondition())
            continue;
        i->save(doc.child("Objects"));
    }

    for (auto &i : short_life_objs) {
        if (!i->getCondition())
            continue;
        i->save(doc.child("Objects"));
    }

    for (auto &i : long_life_objs) {
        if (!i->getCondition())
            continue;
        i->save(doc.child("Objects"));
    }
}

void SceneT::clear_level() {
    Enemy::setNEnemyes(0);
    medium_life_objs.clear();
    long_life_objs.clear();
    short_life_objs.clear();
    Player::setScore(0);
    Enemy::setNEnemyes(0);
}

void SceneT::draw_all_objects(sf::RenderWindow &window) {
    window.draw(_background);
    _player.draw(window);
    for (auto &i : medium_life_objs) {
        if (!i->getCondition())
            continue;
        i->move(MOVE_UP);
        i->draw(window);
    }

    for (auto &i : short_life_objs) {
        if (!i->getCondition())
            continue;
        i->move(MOVE_UP);
        i->draw(window);
    }

    for (auto &i : long_life_objs) {
        if (!i->getCondition())
            continue;
        i->move(MOVE_UP);
        i->draw(window);
    }
}

const std::size_t SceneT::getScore() const {
    return _score;
}

const sf::Sprite &SceneT::getBackground() const {
    return _background;
}

